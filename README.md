# Hra snake
## Autor: Jakub Drápalík
## ČVUT FEL PCC



## Popis zadání

Cílem tohoto projektu bylo vytvořit klasickou hru Snake, kterou můžeme znát například ze staré Nokie.
Hra běží ve vláknech a umožňuje hráči ovládat hada pomocí kláves WSAD nebo šipek na klávesnici. 
Cílem hry je sníst co nejvíce ovoce a dosáhnout co nejvyššího skóre.
Hra končí, když se had dotkne ohraničení hranic nebo svého vlastního těla.


## Popis implmentace

### Třída SnakeGame

- Obsahuje informace o hadovi (snake), pozici ovoce (fruit), směr pohybu hada (dir), stav hry (gameOver), šířku a výšku herního pole (width a height), aktuální skóre (score), a další proměnné pro multivláknový běh hry.
- Obsahuje metody pro generování ovoce, detekci kolizí, aktualizaci stavu hry, vykreslování herního pole a změnu směru hada.
- Má metody pro spuštění jednotlivých vláken (start, inputThreadFunc, computeThreadFunc, renderThreadFunc).

### Vstupní metody _kbhit a _getch

- Slouží k detekci stisknutí kláves a načtení znaku z klávesnice. Umožňují asynchronní získávání vstupu.

### Multi-thread přístup

- Využívá tři vlákna pro oddělené zpracování vstupu, aktualizaci stavu a vykreslování hry.
- Komunikace mezi vlákny probíhá pomocí atomické proměnné inputReceived a podmíněné proměnné cv.

### Detekce kolizí

- Hra kontroluje kolize hada s okrajem herního pole a sám se sebou.

### Ovládání hry

- Hráč může ovládat směr hada pomocí kláves WSAD nebo šipek.
- Klávesou 'X' lze ukončit hru.

### Vykreslování herního pole

- Používá ANSI escape sekvence pro vyčištění terminálu.
- Vykresluje herní pole s hadem, ovocem, hranicemi a skóre.

## Funknčnost

- Hra začíná s hadem uprostřed pole a s vygenerovaným ovocem.
- Hráč může ovládat hada, sbírat ovoce a sledovat své skóre.
- Konec hry je vyvolán kolizí s okrajem pole nebo hadem samým.
- Hra je implementována tak, aby byla přehledná, interaktivní a umožňovala snadné ovládání.
- Hru lze spustit pouze na Linuxu

## Ovládání

- W nebo šipka nahoru: Pohyb hada nahoru
- S nebo šipka dolů: Pohyb hada dolů
- A nebo šipka vlevo: Pohyb hada vlevo
- D nebo šipka vpravo: Pohyb hada vpravo
- X: Ukončení hry

## Poznámky

- Hra využívá terminálové grafické rozhraní pro vykreslování a interakci.
- Kódy kláves jsou zpracovávány asynchronně ve vlákně pro vstup.
- Vlákna jsou synchronizována pomocí mutexů a podmíněných proměnných.
- Program je napsán v jazyce C++ s využitím standardní knihovny a vláken pro multivláknové zpracování.
- Hra byla rovnou psána tak, aby využívala více vláken, tudíž nebyla implmentována pomocí jednoho vlákna (nemohu poskytnout srovnání).