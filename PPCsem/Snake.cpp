#include <iostream>
#include <vector>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <chrono>
#include <cstdlib>
#include <termios.h>
#include <unistd.h>
#include <cstring>

// ANSI escape sekvence pro vyčištění terminálu
const char* CLEAR_SCREEN = "\x1B[2J\x1B[H";

// Enum směru
enum class Direction { UP, DOWN, LEFT, RIGHT };

// Bod na mřížce
struct Point {
    int x, y;
};

// Třída s hrou
class SnakeGame {
private:
    std::vector<Point> snake; // Tělo hada
    Point fruit; // Pozice ovoce
    Direction dir; // Momentální směr
    bool gameOver;
    int width, height;
    int score;
    std::atomic<bool> inputReceived;
    std::condition_variable cv;
    std::mutex mtx;
    std::thread inputThread;
    std::thread computeThread;
    std::thread renderThread;

public:
    SnakeGame(int w, int h) : width(w), height(h), gameOver(false), score(0), dir(Direction::RIGHT) {
        // Umístí hada doprostřed pole
        snake.push_back({width / 2, height / 2});
        // Položí ovoce
        generateFruit();
        // Nastavení input flagu
        inputReceived = false;
    }

    void generateFruit() {
        // Generování pozice pro ovoce

        do {
            // Ošetření, aby se ovoce nespawnovalo ve zdi
            fruit = {1 + rand() % (width - 2), 1 + rand() % (height - 2)};
        } while (fruitCollidesWithSnake());
    }

    bool fruitCollidesWithSnake() {
        // Kontrola, zda nové ovoce koliduje s tělem hada
        for (const auto& part : snake) {
            if (fruit.x == part.x && fruit.y == part.y) {
                return true;  // Ovoce koliduje, generovat novou pozici
            }
        }
        return false;  // Ovoce nekoliduje s hadem
    }

    // Detekce non-blocking inputu z klávesnice
    static int _kbhit() {
        struct timeval tv;
        fd_set rdfs;
        int ret;

        FD_ZERO(&rdfs);
        FD_SET(STDIN_FILENO, &rdfs);

        tv.tv_sec = 0;
        tv.tv_usec = 0;

        ret = select(STDIN_FILENO + 1, &rdfs, NULL, NULL, &tv);

        return ret > 0 ? 1 : 0;
    }

    // Načtení inputu z klávesnice
    static int _getch() {
        struct termios oldt, newt;
        int ch;
        tcgetattr(STDIN_FILENO, &oldt);
        newt = oldt;
        newt.c_lflag &= ~(ICANON | ECHO);
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
        ch = getchar();
        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
        return ch;
    }

    void update() {
        // Pohyb hlavy hada
        Point newHead = snake.front();
        switch (dir) {
            case Direction::UP:   newHead.y--; break;
            case Direction::DOWN: newHead.y++; break;
            case Direction::LEFT: newHead.x--; break;
            case Direction::RIGHT: newHead.x++; break;
        }

        // GameOver 1 - kolize se zdí
        if (newHead.x < 1 || newHead.y < 1 || newHead.x >= width-1 || newHead.y >= height-1) {
            gameOver = true;
            std::cout << "GAMEOVER: Snake collided with boundary." << std::endl;
            disableRawMode();
            exit(1);
        }

        // GameOver 2 - kolize hlavy a těla hada
        for (const auto& part : snake) {
            if (newHead.x == part.x && newHead.y == part.y) {
                gameOver = true;
                std::cout << "GAMEOVER: Snake collided with its body." << std::endl;
                disableRawMode();
                exit(1);
            }
        }

        // Pohyb hada
        {
            std::lock_guard<std::mutex> lock(mtx);
            snake.insert(snake.begin(), newHead);
        }

        // Kolize hada a ovoce
        {
            std::lock_guard<std::mutex> lock(mtx);
            if (newHead.x == fruit.x && newHead.y == fruit.y) {
                score += 10; // přičíst skore
                generateFruit(); // položit ovoce
            } else {
                // Odebrat ocas
                snake.pop_back();
            }
        }
    }

    void render() {
        std::cout << CLEAR_SCREEN;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1) {
                    std::cout << "#"; // Zdi
                } else if (x == fruit.x && y == fruit.y) {
                    std::cout << "*"; // Ovoce
                } else {
                    bool isBodyPart = false;
                    for (const auto& part : snake) {
                        if (part.x == x && part.y == y) {
                            std::cout << "O"; // Segment hada
                            isBodyPart = true;
                            break;
                        }
                    }
                    if (!isBodyPart) std::cout << " ";
                }
            }
            std::cout << std::endl;
        }
        std::cout << "Score: " << score << " | (X) Exit Game" << std::endl;

        std::cout << std::flush; // Flush bufferu výstupu
    }

    void changeDirection(Direction newDir) {
        // Změna směru hada, přičemž nemůže náhle zvolit opačný směr
        if ((dir == Direction::UP && newDir != Direction::DOWN) ||
            (dir == Direction::DOWN && newDir != Direction::UP) ||
            (dir == Direction::LEFT && newDir != Direction::RIGHT) ||
            (dir == Direction::RIGHT && newDir != Direction::LEFT)) {
            dir = newDir;
        }
    }

    // Vypnutí raw modu terminálu
    void disableRawMode() {
        struct termios term;
        tcgetattr(STDIN_FILENO, &term);
        term.c_lflag |= ICANON | ECHO;
        tcsetattr(STDIN_FILENO, TCSAFLUSH, &term);
    }

    // Vlákno pro vstup
    void inputThreadFunc() {
        Direction lastDir = dir;
        while (!gameOver) {
            if (_kbhit()) {
                int ch = _getch();
                switch (ch) { // Přiřazení akce ke klávese
                    case 'w':
                    case 'W':
                        changeDirection(Direction::UP);
                        lastDir = Direction::UP;
                        break;
                    case 's':
                    case 'S':
                        changeDirection(Direction::DOWN);
                        lastDir = Direction::DOWN;
                        break;
                    case 'a':
                    case 'A':
                        changeDirection(Direction::LEFT);
                        lastDir = Direction::LEFT;
                        break;
                    case 'd':
                    case 'D':
                        changeDirection(Direction::RIGHT);
                        lastDir = Direction::RIGHT;
                        break;
                    case 'x':
                    case 'X':
                    {
                        std::lock_guard<std::mutex> lock(mtx);
                        gameOver = true;  // Ukončit hru
                        disableRawMode();
                        exit(1);
                    }
                    case 27: // Šipky na klávesnici
                        _getch();
                        switch (_getch()) {
                            case 'A': // Nahoru
                                changeDirection(Direction::UP);
                                lastDir = Direction::UP;
                                break;
                            case 'B': // Dolů
                                changeDirection(Direction::DOWN);
                                lastDir = Direction::DOWN;
                                break;
                            case 'C': // Vpravo
                                changeDirection(Direction::RIGHT);
                                lastDir = Direction::RIGHT;
                                break;
                            case 'D': // Vlevo
                                changeDirection(Direction::LEFT);
                                lastDir = Direction::LEFT;
                                break;
                        }
                        break;
                }
                // Komunikace s ostatními vlákny (input flag)
                inputReceived.store(true, std::memory_order_release);
                cv.notify_one();
            } else {
                changeDirection(lastDir); // Bez vstupu se had pohybuje stejným směrem
                inputReceived.store(true, std::memory_order_release);
                cv.notify_one();
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }

    // update vlákno
    void computeThreadFunc() {
        while (!gameOver) {
            {
                std::unique_lock<std::mutex> lock(mtx);
                cv.wait(lock, [this] { return inputReceived.load(std::memory_order_acquire); }); // Čekáme na input flag
            }
            update();
            {
                std::lock_guard<std::mutex> lock(mtx);
                inputReceived.store(false, std::memory_order_release);
            }
        }
    }

    // render vlákno
    void renderThreadFunc() {
        while (!gameOver) {
            {
                std::unique_lock<std::mutex> lock(mtx);
                cv.wait(lock, [this] { return inputReceived.load(std::memory_order_acquire); }); // Čekáme na input flag
            }
            render();
            {
                std::lock_guard<std::mutex> lock(mtx);
                inputReceived.store(false, std::memory_order_release);
            }
        }
    }

    // inicializace vláken a hry
    void start() {
        inputThread = std::thread(&SnakeGame::inputThreadFunc, this);
        computeThread = std::thread(&SnakeGame::computeThreadFunc, this);
        renderThread = std::thread(&SnakeGame::renderThreadFunc, this);

        while (!gameOver) {
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        if (inputThread.joinable()) {
            inputThread.join();
        }
        if (computeThread.joinable()) {
            computeThread.join();
        }
        if (renderThread.joinable()) {
            renderThread.join();
        }
    }
};

// přepnutí do raw modu terminálu
void enableRawMode() {
    struct termios term;
    tcgetattr(STDIN_FILENO, &term);
    term.c_lflag &= ~(ICANON | ECHO);
    term.c_cc[VMIN] = 1;
    term.c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &term);
}

int main(int argc, char *argv[]) {
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        std::cout << "Hra Snake\n";
        std::cout << "Toto je implementace klasické hry Snake. Hra se hraje\n";
        std::cout << "v terminálovém okně. Pro ovládání pohybu hada použijte klávesy WSAD nebo šipky,\n";
        std::cout << "klávesou X hru ukončíte. Cílem je nasbírat co největší skóre - sníst co nejvíce ovoce.\n";
        std::cout << "Konec hry nastává, když se had dotkne ohraničení nebo vlastního těla.\n";
        return 0;
    }
    enableRawMode();
    srand(static_cast<unsigned int>(time(nullptr)));
    SnakeGame game(40, 20);
    game.start();
    return 0;
}
